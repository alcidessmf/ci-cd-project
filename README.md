# ci-cd project

Hello, my name is Alcides Martins, this is project is my first CI/CD project!

This project brings the CI/CD concepts into practice, using the python code to update the PostgreSQL database in the Linode server.

Used tools:
GitLab CI/CD;
Docker for run PostgreSQL and pg_admin in the server;
Python with Pandas and SQLAlchemy;
Cron Jobs for executing the python code;

Pipeline:
1. Run docker-compose to build the database on the server;
2. After updating the ETL, commit the project;
3. After commit, the .gitlab-ci.yml starting the pipelines:
    1. Testing connection with the server;
    2. Download new etl.py in the server folder;
    3. Update the cron code. The cron code runs to schedule the run etl.py;
4. The run etl.py to update database table with file .csv;
5. Finished!


Server - Linode
1. Docker and Docker-compose run
    1. PostgreSQL and PG_Admin run in container docker
2. python3 -m venv python >> Install python binaries
3. creating ssh public
4. install Python dependencies 
    - pip; 
    - psycopg2; 
    - numpy; 
    - sqlalchemy; 
    - pandas