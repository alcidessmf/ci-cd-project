import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

df = pd.read_csv('../../database/database.csv', sep='|')

connect = sqlalchemy.create_engine('postgresql+psycopg2://root:postgres@74.207.224.205:5432/postgres')
print(df.head())

df.to_sql(
    name = 'table_text',
    con = connect,
    schema = 'test_dw',
)

## Creating a new dataframe from table
## df = pd.read_sql_table('tablename',connect, columns=["","",""])

## Read table from query sql
## df = pd.read_sql_query("select * from employees",connect)